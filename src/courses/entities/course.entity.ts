import { Column, Entity, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { HoursOfCourse } from '../enums/hours.enum';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'enum',
    enum: HoursOfCourse,
    default: HoursOfCourse.ONESEMESTER,
    nullable: false,
  })
  hours: HoursOfCourse;

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
  })
  lectors: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  marks: Mark[];
}
