import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
  ) {}

  async create(createCourseDto: CreateCourseDto) {
    const course = await this.courseRepository.findOne({
      where: {
        name: createCourseDto.name,
      },
    });

    if (course) {
      throw new BadRequestException('Course with this name already exists');
    }

    return this.courseRepository.save(createCourseDto);
  }

  async findAll() {
    return await this.courseRepository.find({});
  }

  async findOne(id: string) {
    const course = await this.courseRepository
      .createQueryBuilder('courses')
      .leftJoinAndSelect('courses.lectors', 'lector')
      .where('courses.id = :id', { id })
      .getOne();

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return course;
  }

  async update(id: number, updateCourseDto: UpdateCourseDto) {
    const result = await this.courseRepository.update(id, updateCourseDto);
    if (!result.affected) {
      throw new NotFoundException('Course not found');
    }
  }

  async remove(id: number) {
    const result = await this.courseRepository.delete(id);
    if (!result.affected) {
      throw new NotFoundException('Course not found');
    }
  }

  async findCourseMarksById(id: string) {
    const courseWithMarks = await this.courseRepository
      .createQueryBuilder('course')
      .select(['course.id', 'course.name'])
      .leftJoin('course.marks', 'mark')
      .addSelect([
        'mark.mark',
        'mark.studentId',
        'mark.lectorId',
        'mark.student',
        'mark.lector',
      ])
      .leftJoin('mark.student', 'student')
      .addSelect(['student.name'])
      .leftJoin('mark.lector', 'lector')
      .addSelect(['lector.name'])
      .where('course.id = :id', { id })
      .getOne();

    if (!courseWithMarks) {
      throw new NotFoundException('Course not found');
    }

    return courseWithMarks;
  }
}
