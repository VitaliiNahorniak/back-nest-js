import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  HttpStatus,
} from '@nestjs/common';
import { MarksService } from './marks.service';
import { CreateMarkDto } from './dto/create-mark.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ResponseCreateMarkDto } from './swagger-dto/response-create-mark.dto';

@ApiTags('Students')
@UsePipes(new ValidationPipe())
@Controller('students')
export class MarkController {
  constructor(private readonly marksService: MarksService) {}

  @Post('add-mark')
  @ApiOperation({ summary: 'Create student mark' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
    type: ResponseCreateMarkDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed / The mark already exist',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  addMark(@Body() createMarkDto: CreateMarkDto) {
    return this.marksService.addMark(createMarkDto);
  }
}
