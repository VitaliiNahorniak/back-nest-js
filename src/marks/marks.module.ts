import { Module } from '@nestjs/common';
import { MarksService } from './marks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { MarkController } from './marks.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Mark])],
  controllers: [MarkController],
  providers: [MarksService],
  exports: [MarksService],
})
export class MarksModule {}
