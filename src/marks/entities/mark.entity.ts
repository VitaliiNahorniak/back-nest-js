import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'numeric',
    nullable: false,
  })
  mark: number;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'course_id',
  })
  courseId: number;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'student_id',
  })
  studentId: number;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'lector_id',
  })
  lectorId: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
