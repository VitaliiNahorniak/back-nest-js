import { IsNotEmpty, IsNumber, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CoreEntity } from 'src/application/entities/core.entity';

export class ResponseCreateMarkDto extends CoreEntity {
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(100)
  @ApiProperty()
  mark: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  courseId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  studentId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  lectorId: number;
}
