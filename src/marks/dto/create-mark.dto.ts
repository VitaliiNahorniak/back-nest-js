import { IsNotEmpty, IsNumber, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMarkDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(100)
  @ApiProperty()
  mark: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  courseId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  studentId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  lectorId: number;
}
