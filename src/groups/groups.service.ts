import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Group } from './entities/group.entity';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupRepository: Repository<Group>,
  ) {}

  async create(createGroupDto: CreateGroupDto) {
    const group = await this.groupRepository.findOne({
      where: {
        name: createGroupDto.name,
      },
    });

    if (group) {
      throw new BadRequestException('Group with this name already exists');
    }

    return this.groupRepository.save(createGroupDto);
  }

  async findAll() {
    const groups = await this.groupRepository
      .createQueryBuilder('group')
      //.select(['group.id as id', 'group.name as name'])
      .leftJoinAndSelect('group.students', 'students')
      .getMany();

    if (!groups) {
      throw new NotFoundException('Group not found');
    }

    return groups;
  }

  async findOne(id: string) {
    const group = await this.groupRepository
      .createQueryBuilder('group')
      //.select(['group.id as id', 'group.name as name'])
      .leftJoinAndSelect('group.students', 'students')
      .where('group.id = :id', { id })
      .getOne();

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    return group;
  }

  async update(id: number, updateGroupDto: UpdateGroupDto) {
    const result = await this.groupRepository.update(id, updateGroupDto);

    if (!result.affected) {
      throw new NotFoundException('Group not found');
    }
  }

  async remove(id: number) {
    const result = await this.groupRepository.delete(id);

    if (!result.affected) {
      throw new NotFoundException('Group not found');
    }
  }
}
