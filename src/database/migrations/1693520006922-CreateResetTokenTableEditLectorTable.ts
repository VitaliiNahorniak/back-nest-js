import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateResetTokenTableEditLectorTable1693520006922
  implements MigrationInterface
{
  name = 'CreateResetTokenTableEditLectorTable1693520006922';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_fa21194644d188132582b0d1a3"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_67ca379415454fe43871951552"`,
    );
    await queryRunner.query(
      `CREATE TABLE "resetToken" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "token" character varying NOT NULL, "lector_id" integer NOT NULL, CONSTRAINT "REL_b1f0a5e7d9364755a4f728afe5" UNIQUE ("lector_id"), CONSTRAINT "PK_efdfc86409b4d46e535d424d01e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fa21194644d188132582b0d1a3" ON "lector_course" ("lector_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_67ca379415454fe43871951552" ON "lector_course" ("course_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "resetToken" ADD CONSTRAINT "FK_b1f0a5e7d9364755a4f728afe5c" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "resetToken" DROP CONSTRAINT "FK_b1f0a5e7d9364755a4f728afe5c"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_67ca379415454fe43871951552"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_fa21194644d188132582b0d1a3"`,
    );
    await queryRunner.query(`DROP TABLE "resetToken"`);
    await queryRunner.query(
      `CREATE INDEX "IDX_67ca379415454fe43871951552" ON "lector_course" ("course_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fa21194644d188132582b0d1a3" ON "lector_course" ("lector_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }
}
