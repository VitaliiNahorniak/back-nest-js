import { MigrationInterface, QueryRunner } from 'typeorm';

export class EditLectorTableNameColumnNullable1693488586731
  implements MigrationInterface
{
  name = 'EditLectorTableNameColumnNullable1693488586731';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" ALTER COLUMN "name" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" ALTER COLUMN "name" SET NOT NULL`,
    );
  }
}
