export interface ResetTokenInterface {
  lectorId: number;
  token: string;
}
