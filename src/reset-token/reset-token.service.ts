import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import crypto from 'crypto';

import { ResetTokenInterface } from './interfaces/reset-token.interface';
import { LectorsService } from '../lectors/lectors.service';
import { ResetToken } from './entities/reset-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ResetTokenService {
  private logger: Logger;
  constructor(
    private lectorsService: LectorsService,
    @InjectRepository(ResetToken)
    private resetTokenRepository: Repository<ResetToken>,
  ) {
    this.logger = new Logger(ResetTokenService.name);
  }

  public async generateResetToken(email: string): Promise<ResetTokenInterface> {
    const token = crypto.randomBytes(32).toString('hex');

    const lector = await this.lectorsService.findOneByEmail(email);

    if (!lector) {
      throw new NotFoundException('User not found');
    }

    const existResetToken = await this.resetTokenRepository.findOne({
      where: {
        lectorId: +lector.id,
      },
    });

    if (existResetToken) {
      throw new BadRequestException(
        'Reset token with this user already exists',
      );
    }

    const resetPasswordObject = {
      lectorId: +lector.id,
      token: token,
    };

    await this.resetTokenRepository.save(resetPasswordObject);

    return resetPasswordObject;
  }

  public async getResetToken(token: string): Promise<ResetTokenInterface> {
    const ResetToken = await this.resetTokenRepository.findOne({
      where: {
        token: token,
      },
    });

    const resetPasswordResponse = {
      lectorId: ResetToken.lectorId,
      token: ResetToken.token,
    };

    return resetPasswordResponse;
  }

  public async removeResetToken(token: string): Promise<void> {
    const ResetToken = await this.resetTokenRepository.findOne({
      where: {
        token: token,
      },
    });

    const result = await this.resetTokenRepository.delete(ResetToken.id);

    if (!result.affected) {
      throw new NotFoundException('Token not found');
    }
  }
}
