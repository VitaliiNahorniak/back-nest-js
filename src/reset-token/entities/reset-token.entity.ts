import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'resetToken' })
export class ResetToken extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  token: string;

  @Column({
    type: 'numeric',
    nullable: false,
    name: 'lector_id',
  })
  lectorId: number;

  @OneToOne(() => Lector, (lector) => lector.resetToken, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
