import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { CoreEntity } from 'src/application/entities/core.entity';

export class ResponseCreateStudentDto extends CoreEntity {
  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  surname: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  age: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  imagePath: string;

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional()
  groupId?: number;
}
