import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from './entities/student.entity';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  async create(createStudentDto: CreateStudentDto) {
    const student = await this.studentRepository.findOne({
      where: {
        email: createStudentDto.email,
      },
    });

    if (student) {
      throw new BadRequestException('Student with this email already exists');
    }

    return this.studentRepository.save(createStudentDto);
  }

  async findAll(name?: string | undefined) {
    if (name) {
      const students = await this.studentRepository
        .createQueryBuilder('student')
        .select([
          'student.id as id',
          'student.name as name',
          'student.surname as surname',
          'student.email as email',
          'student.age as age',
          'student.imagePath as imagePath',
        ])
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .where('student.name = :name', { name })
        .getRawOne();

      if (!students) {
        throw new NotFoundException('Student not found');
      }

      return students;
    } else {
      return this.studentRepository.find({});
    }
  }

  async findOne(id: string) {
    const student = await this.studentRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
        'student.imagePath as imagePath',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .getRawOne();

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    return student;
  }

  async update(id: number, updateStudentDto: UpdateStudentDto) {
    const result = await this.studentRepository.update(id, updateStudentDto);

    if (!result.affected) {
      throw new NotFoundException('Student not found');
    }
  }

  async remove(id: number) {
    const result = await this.studentRepository.delete(id);

    if (!result.affected) {
      throw new NotFoundException('Student not found');
    }
  }

  async findStudentMarksById(id: string) {
    const studentWithMarks = await this.studentRepository
      .createQueryBuilder('student')
      .select(['student.id', 'student.name', 'student.surname'])
      .leftJoin('student.marks', 'mark')
      .addSelect(['mark.mark', 'mark.courseId', 'mark.course'])
      .leftJoin('mark.course', 'course')
      .addSelect(['course.name'])
      .where('student.id = :id', { id })
      .getOne();

    if (!studentWithMarks) {
      throw new NotFoundException('Student not found');
    }

    return studentWithMarks;
  }
}
