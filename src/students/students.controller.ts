import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UsePipes,
  ValidationPipe,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { StudentsService } from './students.service';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ResponseCreateStudentDto } from './swagger-dto/response-student.dto';
import { ResponseStudentIdDto } from './swagger-dto/response-student-id.dto';
import { ResponseStudentIdWithMarksDto } from './swagger-dto/response-student-id-with-marks.dto';
import { AuthGuard } from '../auth/guards/auth.guard';

@ApiTags('Students')
@UsePipes(new ValidationPipe())
@Controller('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @Post()
  @ApiOperation({ summary: 'Create new student' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Success',
    type: ResponseCreateStudentDto,
  })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Bad Request' })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  create(@Body() createStudentDto: CreateStudentDto) {
    return this.studentsService.create(createStudentDto);
  }

  @Get()
  @ApiOperation({ summary: 'Get all students' })
  @ApiQuery({ name: 'name', required: false, description: 'Student name' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    isArray: true,
    type: ResponseCreateStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  findAll(@Query('name') name?: string | undefined) {
    return this.studentsService.findAll(name);
  }

  @Get(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @ApiOperation({ summary: 'Get student by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    type: ResponseStudentIdDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  findOne(@Param('id') id: string) {
    return this.studentsService.findOne(id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'Update student by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The student was updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'The student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  update(@Param('id') id: string, @Body() updateStudentDto: UpdateStudentDto) {
    return this.studentsService.update(+id, updateStudentDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'Delete student by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The student was updated',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'The student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  remove(@Param('id') id: string) {
    return this.studentsService.remove(+id);
  }

  @Get('/:id/marks')
  @ApiOperation({ summary: 'Get student by id with marks' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Success',
    type: ResponseStudentIdWithMarksDto,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'The student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
  })
  findStudentMarksById(@Param('id') id: string) {
    return this.studentsService.findStudentMarksById(id);
  }
}
