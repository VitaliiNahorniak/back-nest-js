import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Lectors')
@UsePipes(new ValidationPipe())
@Controller('lectors')
export class LectorsController {
  constructor(private readonly lectorsService: LectorsService) {}

  @Post()
  create(@Body() createLectorDto: CreateLectorDto) {
    return this.lectorsService.create(createLectorDto);
  }

  @Get()
  findAll() {
    return this.lectorsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.lectorsService.findOne(id);
  }

  @Get('/:id/only-courses')
  findOneOnlyCourses(@Param('id') id: string) {
    return this.lectorsService.findOneOnlyCourses(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLectorDto: UpdateLectorDto) {
    return this.lectorsService.update(+id, updateLectorDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.lectorsService.remove(+id);
  }
}
