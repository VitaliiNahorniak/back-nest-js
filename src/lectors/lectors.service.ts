import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Lector } from './entities/lector.entity';
import * as argon2 from 'argon2';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
  ) {}

  async create(createLectorDto: CreateLectorDto) {
    const existLector = await this.lectorRepository.findOne({
      where: {
        email: createLectorDto.email,
      },
    });

    if (existLector) {
      throw new BadRequestException('Lector with this email already exists');
    }

    const lector = await this.lectorRepository.save({
      name: createLectorDto.name,
      email: createLectorDto.email,
      password: await argon2.hash(createLectorDto.password),
    });

    return lector;
  }

  async findAll() {
    return await this.lectorRepository.find({});
  }

  async findOne(id: string) {
    const lector = await this.lectorRepository.findOneBy({ id });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async findOneByEmail(email: string) {
    const lector = await this.lectorRepository.findOneBy({ email });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async findOneOnlyCourses(id: string) {
    const lector = await this.lectorRepository
      .createQueryBuilder('lectors')
      .leftJoinAndSelect('lectors.courses', 'course')
      .where('lectors.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async update(id: number, updateLectorDto: UpdateLectorDto) {
    const result = await this.lectorRepository.update(id, {
      name: updateLectorDto.name,
      email: updateLectorDto.email,
      password: await argon2.hash(updateLectorDto.password),
    });

    if (!result.affected) {
      throw new NotFoundException('Lector not found');
    }
  }

  async remove(id: number) {
    const result = await this.lectorRepository.delete(id);

    if (!result.affected) {
      throw new NotFoundException('Lector not found');
    }
  }
}
