import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetLectorDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;
}
