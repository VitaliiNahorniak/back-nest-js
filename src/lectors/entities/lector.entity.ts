import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ResetToken } from '../../reset-token/entities/reset-token.entity';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: true,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  email: string;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  password: string;

  @ManyToMany(() => Course, (course) => course.lectors, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector, {
    nullable: true,
    onDelete: 'SET NULL',
  })
  marks: Mark[];

  @OneToOne(() => ResetToken, (resetToken) => resetToken.lector, {
    nullable: false,
    eager: false, //automatic upload group - false. We will use for that JOIN
    onDelete: 'CASCADE',
  })
  resetToken: ResetToken;
}
