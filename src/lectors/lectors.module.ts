import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { LectorsController } from './lectors.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Lector])],
  controllers: [LectorsController],
  providers: [LectorsService],
  exports: [LectorsService],
})
export class LectorsModule {}
