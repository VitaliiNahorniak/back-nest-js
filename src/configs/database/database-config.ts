import 'dotenv/config';
import { DataSourceOptions } from 'typeorm';
import * as process from 'process';

export const databaseConfiguration = (
  isMigrationRun = true,
): DataSourceOptions => {
  const ROOT_PATH: string = process.cwd();

  let typeFiles: string;
  let mainFolder: string;

  if (process.env.APP_ENV === 'production') {
    typeFiles = 'js';
    mainFolder = 'dist';
  } else {
    typeFiles = 'ts';
    mainFolder = 'src';
  }

  const migrationsPath = `${ROOT_PATH}/${mainFolder}/**/migrations/*.${typeFiles}`;
  const entitiesPath = `${ROOT_PATH}/${mainFolder}/**/*.entity.${typeFiles}`;

  return {
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities: [entitiesPath],
    migrations: [migrationsPath],
    migrationsTableName: 'migrations',
    migrationsRun: isMigrationRun,
    logging: true,
    synchronize: false,
  };
};
