import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StudentsModule } from '../students/students.module';
import { ConfigModule } from '../configs/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { GroupsModule } from '../groups/groups.module';
import { CoursesModule } from '../courses/courses.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MarksModule } from 'src/marks/marks.module';
import { LectorCourseModule } from '../lector_course/lector-course.module';
import { UsersModule } from '../users/users.module';
import { ResetTokenModule } from '../reset-token/reset-token.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    StudentsModule,
    GroupsModule,
    CoursesModule,
    LectorsModule,
    MarksModule,
    LectorCourseModule,
    UsersModule,
    ResetTokenModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
