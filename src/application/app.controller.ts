import { Controller } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('app')
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get('test')
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  // @Get('get/:id')
  // getGet(@Param('id', ParseIntPipe) id: number) {
  //   if (id < 1) {
  //     throw new BadRequestException('Id should be more than 0');
  //   }
  //   return id;
  // }
}
