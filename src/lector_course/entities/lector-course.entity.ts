import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Lector } from '../../lectors/entities/lector.entity';
import { Course } from '../../courses/entities/course.entity';

@Entity({ name: 'lector_course' })
export class LectorCourse {
  @PrimaryColumn({ name: 'lector_id' })
  lectorId: number;

  @PrimaryColumn({ name: 'course_id' })
  courseId: number;

  @ManyToOne(() => Lector, (lector) => lector.courses, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn([{ name: 'lector_id', referencedColumnName: 'id' }])
  lectors: Lector[];

  @ManyToOne(() => Course, (course) => course.lectors, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn([{ name: 'course_id', referencedColumnName: 'id' }])
  courses: Course[];
}
