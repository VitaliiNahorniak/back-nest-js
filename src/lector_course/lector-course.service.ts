import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateLectorCourseDto } from './dto/create-lector-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LectorCourse } from './entities/lector-course.entity';

@Injectable()
export class LectorCourseService {
  constructor(
    @InjectRepository(LectorCourse)
    private readonly lectorCourseRepository: Repository<LectorCourse>,
  ) {}

  async addCourse(createLectorCourseDto: CreateLectorCourseDto) {
    const lector = await this.lectorCourseRepository.findOne({
      where: {
        lectorId: createLectorCourseDto.lectorId,
        courseId: createLectorCourseDto.courseId,
      },
    });

    if (lector) {
      throw new BadRequestException('Lector with this course already exists');
    }

    return this.lectorCourseRepository.save(createLectorCourseDto);
  }
}
