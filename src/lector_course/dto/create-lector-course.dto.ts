import { IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateLectorCourseDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  lectorId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  courseId: number;
}
