import { Module } from '@nestjs/common';
import { LectorCourseService } from './lector-course.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LectorCourse } from './entities/lector-course.entity';
import { LectorCourseController } from './lector-course.controller';

@Module({
  imports: [TypeOrmModule.forFeature([LectorCourse])],
  controllers: [LectorCourseController],
  providers: [LectorCourseService],
  exports: [LectorCourseService],
})
export class LectorCourseModule {}
