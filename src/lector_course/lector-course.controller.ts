import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { LectorCourseService } from './lector-course.service';
import { CreateLectorCourseDto } from './dto/create-lector-course.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Lectors')
@UsePipes(new ValidationPipe())
@Controller('lectors')
export class LectorCourseController {
  constructor(private readonly lectorCourseService: LectorCourseService) {}

  @Post('/add-course')
  addCourse(@Body() createLectorCourseDto: CreateLectorCourseDto) {
    return this.lectorCourseService.addCourse(createLectorCourseDto);
  }
}
