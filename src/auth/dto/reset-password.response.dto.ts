import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordResponseDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  lectorId: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  token: string;
}
