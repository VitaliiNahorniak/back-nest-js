import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordRequestDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty()
  email: string;
}
