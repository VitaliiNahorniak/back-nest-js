import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { SignResponseDto } from './dto/sign.response.dto';
import { ResetTokenInterface } from '../reset-token/interfaces/reset-token.interface';
import { ResetPasswordWithTokenRequestDto } from './dto/reset-password-with-token.request.dto';
import { LectorsService } from 'src/lectors/lectors.service';
import { CreateLectorDto } from 'src/lectors/dto/create-lector.dto';
import * as argon2 from 'argon2';
import * as nodemailer from 'nodemailer';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
  ) {}

  public async signIn(email: string, pass: string): Promise<SignResponseDto> {
    const user = await this.lectorsService.findOneByEmail(email);

    const passwordIsMatch = await argon2.verify(user?.password, pass);
    if (!passwordIsMatch) {
      throw new UnauthorizedException();
    }

    // const payload = { sub: user.id, username: user.name };
    const payload = { sub: user.id };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async signUp(
    createLectorDto: CreateLectorDto,
  ): Promise<SignResponseDto> {
    const user = await this.lectorsService.create(createLectorDto);
    const payload = { sub: user.id };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async resetPasswordRequest(
    email: string,
  ): Promise<ResetTokenInterface> {
    const user = await this.lectorsService.findOneByEmail(email);
    if (!user) {
      throw new BadRequestException(
        `Cannot generate token for reset password request  because user ${email} is not found`,
      );
    }

    //CREATE NEW SERVICE FOR HTML MESSAGE
    const transporter = nodemailer.createTransport({
      host: 'mailcatcher',
      port: 1025,
      secure: false,
    });

    const htmlMessage =
      '<p>Hello <b>' +
      email +
      '</b></p>' +
      '<p>Someone has requested a link to change your password. You can do this through the link below.</p>' +
      '<p><a href="http://localhost:3000/new-password">Change my password</a></p>' +
      '<p>If you did not request this, please ignore this email.</p>';

    async function main() {
      const info = await transporter.sendMail({
        from: '"nest-server" <nest@server.com>',
        to: email,
        subject: 'Reset password request ✔',
        html: htmlMessage,
      });

      console.log('Message sent: %s', info.messageId);
    }

    main().catch(console.error);

    return await this.resetTokenService.generateResetToken(email);
  }

  public async resetPassword(
    resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    const { token, oldPassword, newPassword } =
      resetPasswordWithTokenRequestDto;

    const resetPasswordRequest =
      await this.resetTokenService.getResetToken(token);

    if (!resetPasswordRequest) {
      throw new BadRequestException(
        `There is no request password reset for current user`,
      );
    }

    const user = await this.lectorsService.findOne(
      `${resetPasswordRequest.lectorId}`,
    );

    if (!user) {
      throw new BadRequestException(`User is not found`);
    }

    const passwordIsMatch = await argon2.verify(user?.password, oldPassword);
    if (!passwordIsMatch) {
      throw new BadRequestException(`Old password is incorrect`);
    }

    await this.lectorsService.update(+user.id, {
      password: newPassword,
    });
    await this.resetTokenService.removeResetToken(token);
  }
}
